# nuxt-steroids

> My splendid Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## Develop with Docker
$ docker-compose -f docker-compose.dev.yml up -d
$ docker logs ${containerId} 
wait npm install package and run if you see "Listening on: http://172.24.0.2:3000" 
you can run localhost:3111 (port in docker-compose file) in your machine

```
